package com.example.drinkappsrepeat.di

import android.app.Application
import com.example.drinkappsrepeat.data.repo.remoteModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidContext(this@MyApplication)
            androidLogger()
            modules(
                listOf(
                    localModule,
                    remoteModule,
                    databaseModule,
                    drinkDetailViewModel,
                    categoryViewModelModule,
                    drinkListViewModelModule
                )
            )
        }
    }
}