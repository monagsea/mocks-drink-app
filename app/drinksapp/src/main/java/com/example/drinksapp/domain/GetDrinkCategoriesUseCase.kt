package com.example.drinkappsrepeat.domain

import com.example.drinkappsrepeat.data.local.entity.Category
import com.example.drinkappsrepeat.data.repo.DrinksRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkCategoriesUseCase(
    private val repo: DrinksRepo
) {
    suspend operator fun invoke(): List<Category> = withContext(Dispatchers.IO) {
        repo.getDrinkCategories()
    }
}
