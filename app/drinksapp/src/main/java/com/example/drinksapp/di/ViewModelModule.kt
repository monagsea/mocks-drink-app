package com.example.drinkappsrepeat.di

import com.example.drinkappsrepeat.data.repo.DrinksRepo
import com.example.drinkappsrepeat.domain.GetDrinkCategoriesUseCase
import com.example.drinkappsrepeat.domain.GetDrinkDetailsUseCase
import com.example.drinkappsrepeat.domain.GetDrinkListByIdUseCase
import com.example.drinkappsrepeat.presentation.viewmodel.CategoryListViewModel
import com.example.drinkappsrepeat.presentation.viewmodel.DrinkDetailViewModel
import com.example.drinkappsrepeat.presentation.viewmodel.DrinkListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val categoryViewModelModule = module {
    viewModel {
        CategoryListViewModel(
            getDrinkCategoriesUseCase = GetDrinkCategoriesUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(),
                    database = provideDatabase(androidContext())
                )
            )
        )
    }
}

val drinkListViewModelModule = module {
    viewModel {
        DrinkListViewModel(
            getDrinkListByIdUseCase = GetDrinkListByIdUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(),
                    database = provideDatabase(androidContext())

                )
            )
        )
    }
}

val drinkDetailViewModel = module {
    viewModel {
        DrinkDetailViewModel(
            getDrinkDetailsUseCase = GetDrinkDetailsUseCase(
                repo = DrinksRepo(
                    service = provideRetrofit(),
                    database = provideDatabase(androidContext())
                )
            )
        )
    }
}
