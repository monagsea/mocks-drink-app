package com.example.drinkappsrepeat.presentation.view.fragment.categoryListFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drinkappsrepeat.databinding.FragmentCategoryListBinding
import com.example.drinkappsrepeat.presentation.view.adapter.CategoryAdapter
import com.example.drinkappsrepeat.presentation.viewmodel.CategoryListViewModel
import org.koin.android.ext.android.get

class CategoryListFragment : Fragment() {

    private var _binding: FragmentCategoryListBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel = get<CategoryListViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with (binding) {
            rvCategories.layoutManager = LinearLayoutManager(context)
            rvCategories.adapter = categoryAdapter.apply {
                with(categoryViewModel) {
                    browseCategories()
                    categoryList.observe(viewLifecycleOwner) {
                        addCategories(it)
                    }
                }
            }
        }
    }
}