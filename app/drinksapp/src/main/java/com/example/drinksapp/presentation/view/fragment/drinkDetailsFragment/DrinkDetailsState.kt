package com.example.drinkappsrepeat.presentation.view.fragment.drinkDetailsFragment

import com.example.drinkappsrepeat.data.local.entity.Drink
import com.example.drinkappsrepeat.data.remote.response.DrinkDetailDTO

data class DrinkDetailsState(
    val details: List<DrinkDetailDTO.Drink> = emptyList()
)
