package com.example.drinkappsrepeat.di

import android.content.Context
import androidx.room.Room
import com.example.drinkappsrepeat.data.local.DrinksDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { provideDatabase(this.androidContext()) }
}

private const val DB_NAME = "drinks.db"

@Volatile
private var instance: DrinksDatabase? = null

private fun databaseBuilder(context: Context): DrinksDatabase = Room.databaseBuilder(
    context, DrinksDatabase::class.java, DB_NAME
).build()

fun provideDatabase(context: Context): DrinksDatabase {
    return instance ?: databaseBuilder (context).also {
        instance = it
    }
}


