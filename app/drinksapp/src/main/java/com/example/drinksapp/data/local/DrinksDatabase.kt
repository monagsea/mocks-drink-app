package com.example.drinkappsrepeat.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.drinkappsrepeat.data.local.dao.CategoryDao
import com.example.drinkappsrepeat.data.local.dao.DrinkDao
import com.example.drinkappsrepeat.data.local.dao.DrinkListDao
import com.example.drinkappsrepeat.data.local.entity.Category
import com.example.drinkappsrepeat.data.local.entity.Drink
import com.example.drinkappsrepeat.data.local.entity.DrinkList

@Database(entities = [Category::class, Drink::class, DrinkList::class], version = 1)
abstract class DrinksDatabase: RoomDatabase() {

    abstract fun categoryDao(): CategoryDao
    abstract fun userDao(): DrinkListDao
    abstract fun drinkDao(): DrinkDao

}