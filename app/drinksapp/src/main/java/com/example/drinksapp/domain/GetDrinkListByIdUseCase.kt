package com.example.drinkappsrepeat.domain

import com.example.drinkappsrepeat.data.repo.DrinksRepo
import com.example.drinkappsrepeat.data.remote.response.CategoryDrinksDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkListByIdUseCase(
    private val repo: DrinksRepo
) {

    suspend operator fun invoke(category: String): CategoryDrinksDTO = withContext(Dispatchers.IO) {
        repo.getDrinksByCategory(category)
    }

}
