package com.example.drinkappsrepeat.presentation.view.fragment.categoryListFragment

import com.example.drinkappsrepeat.data.local.entity.Category

data class CategoryListState(
    val categories: List<Category> = emptyList()
)
