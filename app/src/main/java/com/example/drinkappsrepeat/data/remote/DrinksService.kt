package com.example.drinkappsrepeat.data.remote

import com.example.drinkappsrepeat.data.remote.response.CategoryDrinksDTO
import com.example.drinkappsrepeat.data.remote.response.CategoryListDTO
import com.example.drinkappsrepeat.data.remote.response.DrinkDetailDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinksService {

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query("c") type: String = "list"): CategoryListDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun getDrinksByCategory(@Query("c") category: String): CategoryDrinksDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetailsById(@Query("i") id: String): DrinkDetailDTO



}