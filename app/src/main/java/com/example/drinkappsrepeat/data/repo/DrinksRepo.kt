package com.example.drinkappsrepeat.data.repo

import com.example.drinkappsrepeat.data.local.DrinksDatabase
import com.example.drinkappsrepeat.data.local.entity.Category
import com.example.drinkappsrepeat.data.remote.DrinksService
import com.example.drinkappsrepeat.data.remote.response.CategoryDrinksDTO
import com.example.drinkappsrepeat.data.remote.response.DrinkDetailDTO
import com.example.drinkappsrepeat.di.provideDatabase
import com.example.drinkappsrepeat.di.provideRetrofit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val remoteModule = module {
    factory { DrinksRepo(provideRetrofit(), provideDatabase(androidContext())) }
}

class DrinksRepo(private val service: DrinksService, database: DrinksDatabase) {

    private val categoryDao = database.categoryDao()

    suspend fun getDrinkCategories(): List<Category> = withContext(Dispatchers.IO) {
        return@withContext categoryDao.getCategories().ifEmpty {
            val categoryList: List<Category> = service.getCategories().categoryItems
                .map {
                    Category(
                        category_name = it.strCategory
                    )
                }
            categoryDao.insertCategory(categoryList)
            return@ifEmpty categoryList
        }
    }

    suspend fun getDrinksByCategory(category: String): CategoryDrinksDTO =
        withContext(Dispatchers.IO) {
            service.getDrinksByCategory(category)
        }

    suspend fun getDrinkDetailsById(id: String): DrinkDetailDTO = withContext(Dispatchers.IO) {
        service.getDrinkDetailsById(id)
    }


}