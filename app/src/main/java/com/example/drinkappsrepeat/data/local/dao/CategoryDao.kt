package com.example.drinkappsrepeat.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.drinkappsrepeat.data.local.entity.Category

@Dao
interface CategoryDao {
    @Query("SELECT * FROM Category")
    suspend fun getCategories(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(category: List<Category>)
}