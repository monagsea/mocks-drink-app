package com.example.drinkappsrepeat.di

import com.example.drinkappsrepeat.data.remote.DrinksService
import org.koin.dsl.module

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val localModule = module {
    single { provideRetrofit() }
}

private const val BASE_URL = "https://thecocktaildb.com"

fun provideRetrofit(): DrinksService {


    val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    return retrofit.create(DrinksService::class.java)
}
