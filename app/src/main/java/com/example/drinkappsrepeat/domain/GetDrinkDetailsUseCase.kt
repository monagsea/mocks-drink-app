package com.example.drinkappsrepeat.domain

import com.example.drinkappsrepeat.data.repo.DrinksRepo
import com.example.drinkappsrepeat.data.remote.response.DrinkDetailDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetDrinkDetailsUseCase (
    private val repo: DrinksRepo
) {
    suspend operator fun invoke(id: String): DrinkDetailDTO = withContext(Dispatchers.IO) {
        repo.getDrinkDetailsById(id)
    }
}
