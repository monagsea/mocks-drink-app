package com.example.drinkappsrepeat.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.drinkappsrepeat.data.remote.response.CategoryDrinksDTO
import com.example.drinkappsrepeat.domain.GetDrinkListByIdUseCase
import com.example.drinkappsrepeat.presentation.view.fragment.drinkListFragment.DrinkListState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


class DrinkListViewModel (
    private val getDrinkListByIdUseCase: GetDrinkListByIdUseCase
) : ViewModel() {

    private var _drinkListState = MutableLiveData(DrinkListState())
    val drinkListState: LiveData<DrinkListState> get() = _drinkListState

    private var _drinkList = MutableLiveData<CategoryDrinksDTO>()
    val drinkList: LiveData<CategoryDrinksDTO> get() = _drinkList

    fun getDrinksById(category: String) {
        viewModelScope.launch {
            _drinkListState.value =
                DrinkListState(drinks = getDrinkListByIdUseCase.invoke(category).drinks)
            _drinkList.value = getDrinkListByIdUseCase.invoke(category)
        }
    }

}