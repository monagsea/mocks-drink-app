package com.example.drinkappsrepeat.presentation.view.fragment.drinkListFragment

import com.example.drinkappsrepeat.data.remote.response.CategoryDrinksDTO

data class DrinkListState(
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)
